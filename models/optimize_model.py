import os
import math
import datetime
import pickle
import json
from pathlib import Path

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import cv2

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()

from tensorflow import keras
from tensorflow.keras import layers, models, optimizers, regularizers, callbacks
from tensorflow.compat.v1.keras import backend as K
from tensorflow.keras.preprocessing.image import ImageDataGenerator

import ConfigSpace as CS
import ConfigSpace.hyperparameters as CSH

import hpbandster.core.nameserver as hpns
import hpbandster.core.result as hpres
import hpbandster.visualization as hpvis
from hpbandster.core.worker import Worker
from hpbandster.optimizers import BOHB

from models.clr_callback import CyclicLR
from robopush.utils import Namespace

# import logging
# logging.basicConfig(level=logging.DEBUG)


# Helper function for reformatting output of multi-output generator
def multi_output_generator(gen):
    while True:
        x, y = next(gen)
        y = [y_col for y_col in y.T] if y.ndim > 1 else [y]
        yield x, y

def main():
    m = Namespace()

    # Data dirs
    m.root_dir = Path(os.environ["DATAPATH"])
    m.train_data_dir = m.root_dir/"dynamics/data/surface3d_sliding/data_02031056"
    m.valid_data_dir = m.root_dir/"dynamics/data/surface3d_sliding/data_02031220"
    m.model_dir = m.train_data_dir/("models/opt_" + datetime.datetime.now().strftime("%m%d%H%M"))
    m.train_image_dir = m.train_data_dir/"images"
    m.train_pp_image_dir = m.train_image_dir/"pp_images"
    m.valid_image_dir = m.valid_data_dir/"images"
    m.valid_pp_image_dir = m.valid_image_dir/"pp_images"

    # Sensor image pre-processing params
    m.crop_x, m.crop_y = 115, 25
    m.crop_w, m.crop_h = 430, 430
    m.median_blur_kernel_size = 5
    m.adapt_thresh_block_size = 11
    m.adapt_thresh_offset = -5
    m.resize_w, m.resize_h = 128, 128

    # CNN params
    m.input_dims = (128, 128)
    m.input_channels = 1
    m.output_dim = 3
    m.kernel_size = (3, 3)
    m.padding = 'same'
    m.pool_size = (2, 2)
    m.input_rescale = 1. / 255.
    m.x_col = 'sensor_image'
    m.y_col = ['pose_3', 'pose_4', 'pose_5']
    m.loss_weights = [1.0, 1.0, 1.0]
    m.patience = 10
    m.cyclic_base_lr = 1e-7
    m.cyclic_max_lr = 1e-3
    m.cyclic_step_size = 500
    m.cyclic_mode = 'triangular2'

    # BOHB params
    m.eta = 3
    m.min_budget = 3
    m.max_budget = 100
    m.top_n_percent = 15
    m.n_samples = 64
    m.random_fraction = 0.3333
    m.n_iterations = 50

    # Helper function/closure for pre-processing sensor image
    def preprocess(image):
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        image = image[m.crop_y:m.crop_y+m.crop_h, m.crop_x:m.crop_x+m.crop_w]
        image = cv2.medianBlur(image, m.median_blur_kernel_size)
        image = cv2.adaptiveThreshold(image.astype('uint8'), 255, \
                                      cv2.ADAPTIVE_THRESH_GAUSSIAN_C, \
                                      cv2.THRESH_BINARY, \
                                      m.adapt_thresh_block_size, m.adapt_thresh_offset)
        image = cv2.resize(image, (m.resize_w, m.resize_h), interpolation=cv2.INTER_AREA)
        return np.expand_dims(image, axis=-1)

    # Helper function/closure for pre-processing all sensor images in dir
    def preprocess_dir(src_dir, dest_dir, suffix='.jpg'):
        file_names = (entry for entry in os.listdir(src_dir) if entry.endswith(suffix))
        for file_name in file_names:
            image = cv2.imread(str(src_dir/file_name))
            image = preprocess(image)
            cv2.imwrite(str(dest_dir/file_name), image)

    # Batch pre-process all training images
    if not os.path.exists(m.train_pp_image_dir):
        os.makedirs(m.train_pp_image_dir)
        preprocess_dir(m.train_image_dir, m.train_pp_image_dir)

    # Batch pre-process all validation images
    if not os.path.exists(m.valid_pp_image_dir):
        os.makedirs(m.valid_pp_image_dir)
        preprocess_dir(m.valid_image_dir, m.valid_pp_image_dir)

    # Create missing dirs
    if not os.path.exists(m.model_dir):
        os.makedirs(m.model_dir)

    # Configure GPU
    config = tf.ConfigProto(
        gpu_options=tf.GPUOptions(per_process_gpu_memory_fraction=0.8)
        # device_count = {'GPU': 1}
    )
    config.gpu_options.allow_growth = True
    session = tf.Session(config=config)
    K.set_session(session)

    # Specify BOHB optimizer worker
    class BOHBWorker(Worker):
        def __init__(self, **kwargs):
            super().__init__(**kwargs)
            self.trial = 1

        def compute(self, config, budget, **kwargs):
            print(f"trial: {self.trial}")

            # Unpack params
            n_conv_layers = config['n_conv_layers']
            n_conv_filters = config['n_conv_filters']
            n_dense_layers = config['n_dense_layers']
            n_dense_units = config['n_dense_units']
            hidden_activation = config['hidden_activation']
            batch_norm = config['batch_norm']
            dropout = config['dropout']
            l1_norm = config['l1_norm']
            l2_norm = config['l2_norm']
            batch_size = config['batch_size']
            epochs = int(budget)

            print(f"n_conv_layers: {n_conv_layers}")
            print(f"n_conv_filters: {n_conv_filters}")
            print(f"n_dense_layers: {n_dense_layers}")
            print(f"n_dense_units: {n_dense_units}")
            print(f"hidden_activation: {hidden_activation}")
            print(f"batch_norm: {batch_norm}")
            print(f"dropout: {dropout:0.2}")
            print(f"l1_norm: {l1_norm:0.2}")
            print(f"l2_norm: {l2_norm:0.2}")
            print(f"batch_size: {batch_size}")
            print(f"epochs: {epochs}")

            K.clear_session()

            # Specify CNN model
            x = keras.Input(shape=m.input_dims + (m.input_channels,), name='input')
            inp = x
            for i in range(n_conv_layers):
                x = layers.Conv2D(filters=n_conv_filters, kernel_size=m.kernel_size,
                                  padding=m.padding)(x)
                if batch_norm:
                    x = layers.BatchNormalization()(x)
                x = layers.Activation(hidden_activation)(x)
                x = layers.MaxPooling2D(pool_size=m.pool_size)(x)

            x = layers.Flatten()(x)
            for i in range(n_dense_layers):
                if dropout > 0:
                    x = layers.Dropout(dropout)(x)
                x = layers.Dense(units=n_dense_units,
                                 kernel_regularizer=regularizers.l1_l2(l1=l1_norm, l2=l2_norm))(x)
                x = layers.Activation(hidden_activation)(x)

            if dropout > 0:
                x = layers.Dropout(dropout)(x)
            out = [layers.Dense(units=1,
                                name=('output_' + str(i+1)),
                                kernel_regularizer=regularizers.l1_l2(l1=l1_norm, l2=l2_norm))(x)
                   for i in range(m.output_dim)]

            model = models.Model(inp, out)

            # Compile CNN model
            model.compile(optimizer=optimizers.Adam(),
                          loss=['mse', ] * m.output_dim,
                          loss_weights=m.loss_weights,
                          metrics=['mae'],
                          )

            n_params = model.count_params()
            print(f"models params: {n_params:,}")

            # Specify training data generator
            train_datagen = ImageDataGenerator(rescale=m.input_rescale)
            train_df = pd.read_csv(m.train_data_dir/"targets_image.csv")
            train_generator = train_datagen.flow_from_dataframe(
                dataframe=train_df,
                directory=m.train_pp_image_dir,
                x_col=m.x_col,
                y_col=m.y_col,
                batch_size=batch_size,
                seed=42,
                shuffle=True,
                #     class_mode='other',
                class_mode='raw',
                target_size=m.input_dims,
                color_mode='grayscale',
            )
            train_generator = multi_output_generator(train_generator)

            # Specify validation data generator
            valid_datagen = ImageDataGenerator(rescale=m.input_rescale)
            valid_df = pd.read_csv(m.valid_data_dir/"targets_image.csv")
            valid_generator = valid_datagen.flow_from_dataframe(
                dataframe=valid_df,
                directory=m.valid_pp_image_dir,
                x_col=m.x_col,
                y_col=m.y_col,
                batch_size=batch_size,
                seed=42,
                shuffle=True,
                #     class_mode='other',
                class_mode='raw',
                target_size=m.input_dims,
                color_mode='grayscale',
            )
            valid_generator = multi_output_generator(valid_generator)

            # Specify training callbacks
            train_callbacks = [
                callbacks.EarlyStopping(monitor='val_loss', patience=m.patience, verbose=1),
                callbacks.ModelCheckpoint(filepath=m.model_dir/"best_model.h5",
                                          monitor='val_loss', save_best_only=True),
                CyclicLR(base_lr=m.cyclic_base_lr, max_lr=m.cyclic_max_lr,
                         step_size=m.cyclic_step_size, mode=m.cyclic_mode),
            ]

            try:
                history = model.fit(x=train_generator,
                                    steps_per_epoch=math.ceil(train_df.shape[0] / batch_size),
                                    validation_data=valid_generator,
                                    validation_steps=math.ceil(valid_df.shape[0] / batch_size),
                                    epochs=epochs,
                                    verbose=1,
                                    callbacks=train_callbacks)
            except tf.errors.ResourceExhaustedError:
                results = None
                print("Aborted trial: Resource exhausted error\n")
            else:
                loss = np.min(history.history['val_loss'])
                status = 'OK'
                stopped_epoch = len(history.history['val_loss'])
                print(f"loss: {loss:0.2}")
                print(f"stopped epoch: {stopped_epoch}\n")

                results = {
                    'loss': loss,
                    'info': {'status': status,
                             'stopped_epoch': stopped_epoch,
                             'num_params': n_params,
                             'trial': self.trial, }
                }

            self.trial += 1
            return results

        @staticmethod
        def get_configspace():
            cs = CS.ConfigurationSpace()

            n_conv_layers = CSH.UniformIntegerHyperparameter('n_conv_layers', lower=1,
                                                             upper=7, default_value=1)
            n_conv_filters = CSH.UniformIntegerHyperparameter('n_conv_filters', lower=8,
                                                              upper=256, default_value=32, log=True)
            n_dense_layers = CSH.UniformIntegerHyperparameter('n_dense_layers', lower=1,
                                                              upper=5, default_value=1)
            n_dense_units = CSH.UniformIntegerHyperparameter('n_dense_units', lower=8,
                                                             upper=256, default_value=32, log=True)
            hidden_activation = CSH.CategoricalHyperparameter('hidden_activation', ['relu', 'elu'])
            batch_norm = CSH.CategoricalHyperparameter('batch_norm', [False, True])
            dropout = CSH.UniformFloatHyperparameter('dropout', lower=0.0, upper=0.5,
                                                     default_value=0.5, log=False)
            l1_norm = CSH.UniformFloatHyperparameter('l1_norm', lower=1e-6, upper=1e-1,
                                                     default_value=0.01, log=True)
            l2_norm = CSH.UniformFloatHyperparameter('l2_norm', lower=1e-6, upper=1e-1,
                                                     default_value=0.01, log=True)
            batch_size = CSH.UniformIntegerHyperparameter('batch_size', lower=8, upper=256,
                                                          default_value=32, log=True)

            cs.add_hyperparameters([n_conv_layers, n_conv_filters, n_dense_layers,
                                    n_dense_units, hidden_activation, batch_norm,
                                    dropout, l1_norm, l2_norm, batch_size])

            return cs

    # Configure BOHB logger
    result_logger = hpres.json_result_logger(directory=m.model_dir, overwrite=True)

    # Start BOHB nameserver
    run_id = 'bohb'
    host = '127.0.0.1'
    ns = hpns.NameServer(run_id=run_id, host=host, port=None, working_directory=str(m.model_dir))
    ns_host, ns_port = ns.start()

    # Start BOHB (local) worker
    w = BOHBWorker(run_id=run_id, host=host, nameserver=ns_host)
    w.run(background=True)

    # Run BOHB optimizer
    configspace = BOHBWorker.get_configspace()
    bohb = BOHB(configspace=configspace,
                run_id=run_id,
                eta=m.eta,
                min_budget=m.min_budget,
                max_budget=m.max_budget,
                min_points_in_model=2 * len(configspace.get_hyperparameters()),
                top_n_percent=m.top_n_percent,
                num_samples=m.n_samples,
                random_fraction=m.random_fraction,
                host=host,
                nameserver=ns_host,
                result_logger=result_logger,
                working_directory=str(m.model_dir),
                )
    print("Running BOHB optimisation trials ...\n")
    results = bohb.run(n_iterations=m.n_iterations)

    # Save optimisation results
    with open(m.model_dir/"results.pkl", 'wb') as f:
        pickle.dump(results, f)

    # Save optimisation results in CSV format
    trials_df = pd.DataFrame()
    trials = results.get_all_runs()
    id2config = results.get_id2config_mapping()
    for i, trial in enumerate(trials):
        trial_params = id2config[trial['config_id']]['config']
        trial_row = pd.DataFrame(trial_params, index=[i])
        trial_row['tid'] = i + 1
        trial_row['budget'] = trial['budget']
        trial_row['loss'] = trial['loss']
        if trial['info'] is not None:
            trial_row['status'] = trial['info']['status']
            trial_row['stopped_epoch'] = trial['info']['stopped_epoch']
            trial_row['num_params'] = trial['info']['num_params']
        trials_df = pd.concat([trials_df, trial_row], sort=False)
    trials_df.to_csv(m.model_dir/"results.csv", index=False)

    # Shut down BOHB optimizer and nameserver
    bohb.shutdown(shutdown_workers=True)
    ns.shutdown()

    print("Finished!")

    # Print summary of results
    id2config = results.get_id2config_mapping()
    incumbent = results.get_incumbent_id()
    print(f"Best found configuration: {id2config[incumbent]['config']}")
    print(f"A total of {len(id2config.keys())} unique configurations were sampled")
    print("A total of {len(results.get_all_runs()} runs where executed")
    print(f"Total budget corresponds to {(sum([r.budget for r in results.get_all_runs()]) / bohb.max_budget):0.1f} full function evaluations")

    # Analyse results ...
    # Load results from log file
    results = hpres.logged_results_to_HBS_result(m.model_dir)

    # Get all executed runs
    all_runs = results.get_all_runs()

    # Get 'dict' that translates config ids to actual configurations
    id2conf = results.get_id2config_mapping()

    # Get incumbent (best) configuration
    inc_id = results.get_incumbent_id()

    # Find run on the highest budget
    inc_runs = results.get_runs_by_id(inc_id)
    inc_run = inc_runs[-1]

    # Save the config, loss observed during optimization, and any other relevant info
    inc_loss = inc_run.loss
    inc_stopped_epoch = inc_run.info['stopped_epoch']
    inc_config = id2conf[inc_id]['config']
    inc_loss = inc_run.loss
    opt_config = inc_config.copy()
    opt_config['loss'] = inc_loss
    with open(m.model_dir/"opt_config.json", 'w') as f:
        json.dump(opt_config, f)

    # Print best configuration found
    print("Best found configuration:")
    print(inc_config)
    print(f"Achieving loss of {inc_loss}")

    # Plot and save observed losses, grouped by budget
    hpvis.losses_over_time(all_runs)
    plt.savefig(m.model_dir/"losses_over_time.png")

    # Plot and save number of concurrent runs
    hpvis.concurrent_runs_over_time(all_runs)
    plt.savefig(m.model_dir/"concurrent_runs_over_time.png")

    # Plot and save number of finished runs
    hpvis.finished_runs_over_time(all_runs)
    plt.savefig(m.model_dir/"finished_runs_over_time.png")

    # Plot and save Spearman rank correlation coefficients of losses between different budgets.
    hpvis.correlation_across_budgets(results)
    plt.savefig(m.model_dir/"correlation_across_budgets.png")

    # Plot and save performance of configs picked by the models vs. random ones
    hpvis.performance_histogram_model_vs_random(all_runs, id2conf)
    plt.savefig(m.model_dir/"performance_histogram_model_vs_random.png")

    plt.show()

    # Save optimisation meta data to file
    m.save(m.model_dir/"meta.pkl")


if __name__ == '__main__':
    main()
