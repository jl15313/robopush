import os
from pathlib import Path

import numpy as np
import pandas as pd
import cv2


def load_frames(filename):
    frames = []
    cap = cv2.VideoCapture(str(filename))
    try:
        ret, frame = cap.read()
        while ret:
            frames.append(frame)
            ret, frame = cap.read()
    finally:
        cap.release()
    return np.array(frames)

def extract_video_frames(video_dir, video_target_file, image_dir,
                         image_target_file, n_frames, start_frame):
    video_files = [f for f in os.listdir(video_dir) if f.endswith('.mp4')]

    video_target_df = pd.read_csv(video_target_file)
    image_target_df = pd.DataFrame()

    for i, video_file in enumerate(video_files):
        # Load video frames
        video_path = video_dir/video_file
        frames = load_frames(video_path)

        # Copy video target row as the basis for image target row
        image_target_row = video_target_df[
            video_target_df['sensor_video'] == video_path.name]
        image_target_row = image_target_row.rename(
            columns={'sensor_video': 'sensor_image'})
        image_target_row['sensor_image'] = ""

        # Process frames and targets ...
        image_file_base = "image_" + "".join(video_path.stem.split('_')[1:])
        for j in range(start_frame, start_frame + n_frames):
            # Save frame to PNG file
            image_file = image_file_base + f"_{j+1}.jpg"
            cv2.imwrite(str(image_dir/image_file), frames[j])

            # Append corresponding targets to image targets table
            image_target_row['sensor_image'] = image_file
            image_target_df = image_target_df.append(image_target_row)

        # Report progress
        if (i+1) % 10 == 0:
            print(f"Processed video {i+1} of {len(video_files)}")

    # Re-index image targets and save to CSV
    image_target_df = image_target_df.reset_index(drop=True)
    image_target_df.to_csv(image_target_file, index=False)

    print(f"Finished processing {len(video_files)} videos")

def main():
    # Data dirs/files
    root_dir = Path(os.environ["DATAPATH"])
    data_dir = root_dir/"dynamics/data/surface3d_sliding/data_02031220"
    video_dir = data_dir/"videos"
    image_dir = data_dir/"images"
    video_target_file = data_dir/"targets_video.csv"
    image_target_file = data_dir/"targets_image.csv"

    # Frame extraction params
    n_frames = 1
    start_frame = 0

    if not os.path.exists(image_dir):
        os.makedirs(image_dir)
        extract_video_frames(video_dir, video_target_file, image_dir,
                             image_target_file, n_frames, start_frame)


if __name__ == '__main__':
    main()
