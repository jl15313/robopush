import os
import math
import datetime
import pickle
from pathlib import Path

import numpy as np
import pandas as pd
import cv2

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()

from tensorflow.keras import models
from tensorflow.compat.v1.keras import backend as K
from tensorflow.keras.preprocessing.image import ImageDataGenerator

from robopush.utils import Namespace


# Helper function for reformatting output of multi-output generator
def multi_output_generator(gen):
    while True:
        x, y = next(gen)
        y = [y_col for y_col in y.T] if y.ndim > 1 else [y]
        yield x, y

def main():
    m = Namespace()

    # Data dirs
    m.root_dir = Path(os.environ["DATAPATH"])
    m.train_data_dir = m.root_dir/"dynamics/data/surface3d_sliding/data_02031056"
    m.test_data_dir = m.root_dir/"dynamics/data/surface3d_sliding/data_02031220"
    m.model_dir = m.train_data_dir/"models/mdl_02031604"
    m.analysis_dir = m.test_data_dir/("analysis/test3d_" + datetime.datetime.now().strftime("%m%d%H%M"))
    m.train_image_dir = m.train_data_dir/"images"
    m.train_pp_image_dir = m.train_image_dir/"pp_images"
    m.test_image_dir = m.test_data_dir/"images"
    m.test_pp_image_dir = m.test_image_dir/"pp_images"

    # Sensor image pre-processing params
    m.crop_x, m.crop_y = 115, 25
    m.crop_w, m.crop_h = 430, 430
    m.median_blur_kernel_size = 5
    m.adapt_thresh_block_size = 11
    m.adapt_thresh_offset = -5
    m.resize_w, m.resize_h = 128, 128

    # CNN params
    m.input_dims = (128, 128)
    m.input_channels = 1
    m.output_dim = 3
    m.input_rescale = 1./255.
    m.x_col = 'sensor_image'
    m.y_col = ['pose_3', 'pose_4', 'pose_5']
    m.batch_size = 32

    # Helper function/closure for pre-processing sensor image
    def preprocess(image):
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        image = image[m.crop_y:m.crop_y+m.crop_h, m.crop_x:m.crop_x+m.crop_w]
        image = cv2.medianBlur(image, m.median_blur_kernel_size)
        image = cv2.adaptiveThreshold(image.astype('uint8'), 255, \
                                      cv2.ADAPTIVE_THRESH_GAUSSIAN_C, \
                                      cv2.THRESH_BINARY, \
                                      m.adapt_thresh_block_size, m.adapt_thresh_offset)
        image = cv2.resize(image, (m.resize_w, m.resize_h), interpolation=cv2.INTER_AREA)
        return np.expand_dims(image, axis=-1)

    # Helper function/closure for pre-processing all sensor images in dir
    def preprocess_dir(src_dir, dest_dir, suffix='.jpg'):
        file_names = (entry for entry in os.listdir(src_dir) if entry.endswith(suffix))
        for file_name in file_names:
            image = cv2.imread(str(src_dir/file_name))
            image = preprocess(image)
            cv2.imwrite(str(dest_dir/file_name), image)

    # Batch pre-process all training images
    if not os.path.exists(m.train_pp_image_dir):
        os.makedirs(m.train_pp_image_dir)
        preprocess_dir(m.train_image_dir, m.train_pp_image_dir)

    # Batch pre-process all test images
    if not os.path.exists(m.test_pp_image_dir):
        os.makedirs(m.test_pp_image_dir)
        preprocess_dir(m.test_image_dir, m.test_pp_image_dir)

    # Create missing dirs
    if not os.path.exists(m.analysis_dir):
        os.makedirs(m.analysis_dir)

    # Configure GPU
    config = tf.ConfigProto(
        gpu_options=tf.GPUOptions(per_process_gpu_memory_fraction=0.8)
        # device_count = {'GPU': 1}
    )
    config.gpu_options.allow_growth = True
    session = tf.Session(config=config)
    K.set_session(session)
    K.clear_session()

    # Load CNN model
    model = models.load_model(m.model_dir/"best_model.h5")
    model.summary()

    # Specify test data generator
    test_datagen = ImageDataGenerator(rescale=m.input_rescale)
    test_df = pd.read_csv(m.test_data_dir/"targets_image.csv")
    test_generator = test_datagen.flow_from_dataframe(
        dataframe=test_df,
        directory=m.test_pp_image_dir,
        x_col=m.x_col,
        y_col=m.y_col,
        batch_size=m.batch_size,
        seed=42,
        shuffle=False,
        #     class_mode='other',
        class_mode='raw',
        target_size=m.input_dims,
        color_mode='grayscale',
    )
    test_generator = multi_output_generator(test_generator)

    # Evaluate CNN model
    results = model.evaluate(x=test_generator, steps=math.ceil(test_df.shape[0] / m.batch_size))
    results = dict(zip(model.metrics_names, results))
    print(results)
    with open(m.analysis_dir/"results.pkl", 'wb') as f:
        pickle.dump(results, f)

    # Generate test predictions
    test_generator = test_datagen.flow_from_dataframe(
        dataframe=test_df,
        directory=m.test_pp_image_dir,
        x_col=m.x_col,
        batch_size=m.batch_size,
        seed=42,
        shuffle=False,
        class_mode=None,
        target_size=m.input_dims,
        color_mode='grayscale',
    )

    preds = model.predict(x=test_generator, steps=math.ceil(test_df.shape[0] / m.batch_size))
    preds = np.squeeze(np.array(preds)).T
    y_col_pred = [c + '_pred' for c in m.y_col]
    pred_df = pd.DataFrame(preds, columns=y_col_pred)
    print(pred_df.head())

    # Save test predictions to file
    pred_df.to_pickle(m.analysis_dir/"predict.pkl")
    pred_df.to_csv(m.analysis_dir/"predict.csv", index=False)

    # Perform error analysis
    test_df = pd.read_csv(os.path.join(m.test_data_dir, "targets_image.csv"))
    target_df = test_df.loc[:, m.y_col]
    y_col_err = [c + '_err' for c in m.y_col]
    error_df = pd.DataFrame(target_df.values - pred_df.values, columns=y_col_err)
    analysis_df = pd.concat([test_df, pred_df.reindex(test_df.index), error_df.reindex(test_df.index)], axis=1)
    print(analysis_df.head())

    # Save error analysis to file
    analysis_df.to_pickle(m.analysis_dir/"analysis.pkl")
    analysis_df.to_csv(m.analysis_dir/"analysis.csv", index=False)

    # Save testing meta data to file
    m.save(m.analysis_dir/"meta.pkl")


if __name__ == '__main__':
    main()
