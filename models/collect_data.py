import os
import time
from pathlib import Path

import numpy as np
import pandas as pd

import sobol_seq

from cri.robot import SyncRobot, AsyncRobot
from cri.controller import RTDEController

from vsp.video_stream import CvVideoCamera, CvVideoDisplay, CvVideoOutputFile
from vsp.processor import CameraStreamProcessorMT, AsyncProcessor

from robopush.utils import Namespace


# Helper function for Sobol sampling
def sobol_uniform(num_samples=1, dim=1, low=0.0, high=1.0):
    low, high = np.array(low), np.array(high)
    std_unif_samples = sobol_seq.i4_sobol_generate(dim, num_samples)
    sobol_samples = low + (high - low) * std_unif_samples
    return sobol_samples


def main():
    np.random.seed()

    # Data collection meta data
    m = Namespace()

    # Data dirs/files
    m.root_dir = Path(os.environ["DATAPATH"])
    m.data_dir = m.root_dir/("dynamics/data/surface3d_sliding/data_" + time.strftime('%m%d%H%M'))
    m.video_dir = m.data_dir/"videos"
    m.video_target_file = m.data_dir/"targets_video.csv"

    # Pose sampling params
    m.x_rng = [0, 0]
    m.y_rng = [0, 0]
    m.z_rng = [0.5, 4.5]
    m.alpha_rng = [-20, 20]
    m.beta_rng = [-20, 20]
    m.gamma_rng = [0, 0]

    m.x_offset_rng = [-5, 5]
    m.y_offset_rng = [-5, 5]
    m.z_offset_rng = [0, 0]
    m.alpha_offset_rng = [-5, 5]
    m.beta_offset_rng = [-5, 5]
    m.gamma_offset_rng = [-5, 5]

    m.n_samples = 2000

    m.generator = 'random'
    # m.generator = 'sobol'

    # TacTip params
    m.sensor_source = 0
    m.sensor_exposure = -6

    # Robot params
    m.robot_ip = '192.11.72.10'
    m.robot_axes = 'sxyz'
    m.robot_tcp = [0, 0, 79.0, 0, 0, 0]

    # Coord frames and motion params
    m.base_frame = [0, 0, 0, 0, 0, 0]
    m.work_frame = [22, -427, 90, -180, 0, 90]
    m.base_home_pose = [109.3, -487.1, 351.4, -180, 0, 90]
    m.work_start_pose = [0, 0, -10, 0, 0, 0]

    # Create missing dirs
    if not os.path.exists(m.data_dir):
        os.makedirs(m.data_dir)
    if not os.path.isdir(m.video_dir):
        os.makedirs(m.video_dir)

    # Generate random poses
    if m.generator == 'sobol':
        poses = sobol_uniform(m.n_samples, dim=6,
                              low=(m.x_rng[0], m.y_rng[0], m.z_rng[0],
                                   m.alpha_rng[0], m.beta_rng[0], m.gamma_rng[0]),
                              high=(m.x_rng[1], m.y_rng[1], m.z_rng[1],
                                    m.alpha_rng[1], m.beta_rng[1], m.gamma_rng[1]))
    else:
        poses = np.random.uniform(low=(m.x_rng[0], m.y_rng[0], m.z_rng[0],
                                       m.alpha_rng[0], m.beta_rng[0], m.gamma_rng[0]),
                                  high=(m.x_rng[1], m.y_rng[1], m.z_rng[1],
                                        m.alpha_rng[1], m.beta_rng[1], m.gamma_rng[1]),
                                  size=(m.n_samples, 6))
    poses = poses[np.lexsort((poses[:, 4], poses[:, 3], poses[:, 2]))]

    # Generate random offsets in feature frame
    offsets = np.random.uniform(low=(m.x_offset_rng[0], m.y_offset_rng[0], m.z_offset_rng[0],
                                     m.alpha_offset_rng[0], m.beta_offset_rng[0], m.gamma_offset_rng[0]),
                                high=(m.x_offset_rng[1], m.y_offset_rng[1], m.z_offset_rng[1],
                                      m.alpha_offset_rng[1], m.beta_offset_rng[1], m.gamma_offset_rng[1]),
                                size=(m.n_samples, 6))

    # Compile target data (random poses and offsets)
    target_df = pd.DataFrame(columns=['sensor_video', 'pose_id',
                                      'pose_1', 'pose_2', 'pose_3', 'pose_4', 'pose_5', 'pose_6',
                                      'offset_1', 'offset_2', 'offset_3', 'offset_4', 'offset_5', 'offset_6'])
    for i in range(m.n_samples):
        video_file = 'video_{:d}.mp4'.format(i+1)
        target_df.loc[i] = np.hstack(((video_file, i + 1), poses[i, :], offsets[i, :]))

    target_df.to_csv(m.video_target_file, index=False)


    # Helper functions/closures for initialising robot and sensor
    def make_robot():
        return AsyncRobot(SyncRobot(RTDEController(ip=m.robot_ip)))

    def make_sensor():
        return AsyncProcessor(CameraStreamProcessorMT(
            camera=CvVideoCamera(source=m.sensor_source, exposure=m.sensor_exposure),
            display=CvVideoDisplay(name='preview'),
            writer=CvVideoOutputFile(),
        ))

    # Collect the data ...
    with make_robot() as robot, make_sensor() as sensor:
        # Configure robot
        robot.axes = m.robot_axes
        robot.tcp = m.robot_tcp
        robot.linear_speed = 50

        # Skip first few sensor frames to give auto-exposure time to adjust
        sensor.process(num_frames=3)

        # Move to home position wrt base frame
        print("Moving to home position ...")
        robot.coord_frame = m.base_frame
        robot.move_linear(m.base_home_pose)

        # Move to start position wrt work frame
        print("Moving to start position ...")
        robot.coord_frame = m.work_frame
        robot.move_linear(m.work_start_pose)

        # Move a bit faster while collecting data
        robot.linear_speed = 100

        # Iterate over sampled poses ...
        for index, row in target_df.iterrows():
            i_pose = int(row.loc['pose_id'])
            pose = row.loc['pose_1': 'pose_6'].values.astype(np.float)
            offset = row.loc['offset_1': 'offset_6'].values.astype(np.float)
            sensor_video = row.loc['sensor_video']

            print(f"Collecting data for pose {i_pose} ...")

            # Move to offset sampled pose above surface (avoid changing pose in contact with object)
            robot.move_linear(pose + offset + [0, 0, -10, 0, 0, 0])

            # Move to point of contact with surface
            robot.move_linear(pose + offset)

            # Slide in contact to sampled pose
            robot.move_linear(pose)

            # Capture and save sensor image (use second frame to avoid camera double-buffering problem)
            sensor.process(num_frames=1, start_frame=1, outfile=str(m.video_dir/sensor_video))

            # Move clear of surface (avoid changing pose in contact with object)
            robot.move_linear(pose + [0, 0, -10, 0, 0, 0])

        # Move to home position wrt base frame
        print("Moving to home position ...")
        robot.linear_speed = 50
        robot.coord_frame = m.base_frame
        robot.move_linear(m.base_home_pose)

        print("Finished!")

    # Save meta data to file
    m.save(m.data_dir/"meta.pkl")


if __name__ == '__main__':
    main()
