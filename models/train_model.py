import os
import math
import datetime
import pickle
from pathlib import Path

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import cv2

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()

from tensorflow import keras
from tensorflow.keras import layers, models, optimizers, regularizers, callbacks
from tensorflow.compat.v1.keras import backend as K
from tensorflow.keras.preprocessing.image import ImageDataGenerator

from models.clr_callback import CyclicLR
from robopush.utils import Namespace


# Helper function for reformatting output of multi-output generator
def multi_output_generator(gen):
    while True:
        x, y = next(gen)
        y = [y_col for y_col in y.T] if y.ndim > 1 else [y]
        yield x, y

def main():
    m = Namespace()

    # Data dirs
    m.root_dir = Path(os.environ["DATAPATH"])
    m.train_data_dir = m.root_dir/"dynamics/data/surface3d_sliding/data_02031056"
    m.valid_data_dir = m.root_dir/"dynamics/data/surface3d_sliding/data_02031220"
    m.model_dir = m.train_data_dir/("models/mdl_" + datetime.datetime.now().strftime("%m%d%H%M"))
    m.train_image_dir = m.train_data_dir/"images"
    m.train_pp_image_dir = m.train_image_dir/"pp_images"
    m.valid_image_dir = m.valid_data_dir/"images"
    m.valid_pp_image_dir = m.valid_image_dir/"pp_images"

    # Sensor image pre-processing params
    m.crop_x, m.crop_y = 115, 25
    m.crop_w, m.crop_h = 430, 430
    m.median_blur_kernel_size = 5
    m.adapt_thresh_block_size = 11
    m.adapt_thresh_offset = -5
    m.resize_w, m.resize_h = 128, 128

    # CNN params
    m.input_dims = (128, 128)
    m.input_channels = 1
    m.output_dim = 3
    m.kernel_size = (3, 3)
    m.padding = 'same'
    m.pool_size = (2, 2)
    m.n_conv_layers = 7
    m.n_conv_filters = 248
    m.n_dense_layers = 3
    m.n_dense_units = 244
    m.hidden_activation = 'elu'
    m.batch_norm = False
    m.dropout = 0.04596
    m.l1_norm = 1.59e-6
    m.l2_norm = 2.23e-6
    m.input_rescale = 1./255.
    m.x_col = 'sensor_image'
    m.y_col = ['pose_3', 'pose_4', 'pose_5']
    m.batch_size = 18
    m.loss_weights = [1.0, 1.0, 1.0]
    m.patience = 10
    m.epochs = 100
    m.cyclic_base_lr = 1e-7
    m.cyclic_max_lr = 1e-3
    m.cyclic_step_size = 500
    m.cyclic_mode = 'triangular2'

    # Helper function/closure for pre-processing sensor image
    def preprocess(image):
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        image = image[m.crop_y:m.crop_y+m.crop_h, m.crop_x:m.crop_x+m.crop_w]
        image = cv2.medianBlur(image, m.median_blur_kernel_size)
        image = cv2.adaptiveThreshold(image.astype('uint8'), 255, \
                                      cv2.ADAPTIVE_THRESH_GAUSSIAN_C, \
                                      cv2.THRESH_BINARY, \
                                      m.adapt_thresh_block_size, m.adapt_thresh_offset)
        image = cv2.resize(image, (m.resize_w, m.resize_h), interpolation=cv2.INTER_AREA)
        return np.expand_dims(image, axis=-1)

    # Helper function/closure for pre-processing all sensor images in dir
    def preprocess_dir(src_dir, dest_dir, suffix='.jpg'):
        file_names = (entry for entry in os.listdir(src_dir) if entry.endswith(suffix))
        for file_name in file_names:
            image = cv2.imread(str(src_dir/file_name))
            image = preprocess(image)
            cv2.imwrite(str(dest_dir/file_name), image)

    # Batch pre-process all training images
    if not os.path.exists(m.train_pp_image_dir):
        os.makedirs(m.train_pp_image_dir)
        preprocess_dir(m.train_image_dir, m.train_pp_image_dir)

    # Batch pre-process all validation images
    if not os.path.exists(m.valid_pp_image_dir):
        os.makedirs(m.valid_pp_image_dir)
        preprocess_dir(m.valid_image_dir, m.valid_pp_image_dir)

    # Create missing dirs
    if not os.path.exists(m.model_dir):
        os.makedirs(m.model_dir)

    # Configure GPU
    config = tf.ConfigProto(
        gpu_options=tf.GPUOptions(per_process_gpu_memory_fraction=0.8)
        # device_count = {'GPU': 1}
    )
    config.gpu_options.allow_growth = True
    session = tf.Session(config=config)
    K.set_session(session)
    K.clear_session()

    # Specify CNN model
    x = keras.Input(shape=m.input_dims + (m.input_channels,), name='input')
    inp = x
    for i in range(m.n_conv_layers):
        x = layers.Conv2D(filters=m.n_conv_filters, kernel_size=m.kernel_size,
                          padding=m.padding)(x)
        if m.batch_norm:
            x = layers.BatchNormalization()(x)
        x = layers.Activation(m.hidden_activation)(x)
        x = layers.MaxPooling2D(pool_size=m.pool_size)(x)

    x = layers.Flatten()(x)
    for i in range(m.n_dense_layers):
        if m.dropout > 0:
            x = layers.Dropout(m.dropout)(x)
        x = layers.Dense(units=m.n_dense_units,
                         kernel_regularizer=regularizers.l1_l2(l1=m.l1_norm, l2=m.l2_norm))(x)
        x = layers.Activation(m.hidden_activation)(x)

    if m.dropout > 0:
        x = layers.Dropout(m.dropout)(x)
    out = [layers.Dense(units=1,
                        name=('output_' + str(i+1)),
                        kernel_regularizer=regularizers.l1_l2(l1=m.l1_norm, l2=m.l2_norm))(x)
           for i in range(m.output_dim)]

    model = models.Model(inp, out)

    # Compile CNN model
    model.compile(optimizer=optimizers.Adam(),
                  loss=['mse', ] * m.output_dim,
                  loss_weights=m.loss_weights,
                  metrics=['mae'],
                  )
    model.summary()
    with open(m.model_dir/"network_config.txt", 'w') as f:
        model.summary(print_fn=lambda x: f.write(x + '\n'))

    # Specify training data generator
    train_datagen = ImageDataGenerator(rescale=m.input_rescale)
    train_df = pd.read_csv(m.train_data_dir/"targets_image.csv")
    train_generator = train_datagen.flow_from_dataframe(
        dataframe=train_df,
        directory=m.train_pp_image_dir,
        x_col=m.x_col,
        y_col=m.y_col,
        batch_size=m.batch_size,
        seed=42,
        shuffle=True,
        #     class_mode='other',
        class_mode='raw',
        target_size=m.input_dims,
        color_mode='grayscale',
    )
    train_generator = multi_output_generator(train_generator)

    # Specify validation data generator
    valid_datagen = ImageDataGenerator(rescale=m.input_rescale)
    valid_df = pd.read_csv(m.valid_data_dir/"targets_image.csv")
    valid_generator = valid_datagen.flow_from_dataframe(
        dataframe=valid_df,
        directory=m.valid_pp_image_dir,
        x_col=m.x_col,
        y_col=m.y_col,
        batch_size=m.batch_size,
        seed=42,
        shuffle=True,
        #     class_mode='other',
        class_mode='raw',
        target_size=m.input_dims,
        color_mode='grayscale',
    )
    valid_generator = multi_output_generator(valid_generator)

    # Specify training callbacks
    train_callbacks = [
        callbacks.EarlyStopping(monitor='val_loss', patience=m.patience, verbose=1),
        callbacks.ModelCheckpoint(filepath=m.model_dir/"best_model.h5",
                                  monitor='val_loss', save_best_only=True),
        CyclicLR(base_lr=m.cyclic_base_lr, max_lr=m.cyclic_max_lr,
                 step_size=m.cyclic_step_size, mode=m.cyclic_mode),
    ]

    # Train CNN model
    history = model.fit(x=train_generator,
                        steps_per_epoch=math.ceil(train_df.shape[0] / m.batch_size),
                        validation_data=valid_generator,
                        validation_steps=math.ceil(valid_df.shape[0] / m.batch_size),
                        epochs=m.epochs,
                        verbose=1,
                        callbacks=train_callbacks)

    # Save training history
    with open(m.model_dir/"history.pkl", 'wb') as f:
        pickle.dump(history.history, f)

    # Compute min losses/errors
    min_loss_idx = np.argmin(history.history['val_loss'])
    min_loss = history.history['loss'][min_loss_idx]
    min_mae_1 = history.history['output_1_mean_absolute_error'][min_loss_idx]
    min_mae_2 = history.history['output_2_mean_absolute_error'][min_loss_idx]
    min_mae_3 = history.history['output_3_mean_absolute_error'][min_loss_idx]
    min_val_loss = history.history['val_loss'][min_loss_idx]
    min_val_mae_1 = history.history['val_output_1_mean_absolute_error'][min_loss_idx]
    min_val_mae_2 = history.history['val_output_2_mean_absolute_error'][min_loss_idx]
    min_val_mae_3 = history.history['val_output_3_mean_absolute_error'][min_loss_idx]

    print("Training MSE loss: {:0.4}".format(min_loss))
    print("Training MAE 1: {:0.4}".format(min_mae_1))
    print("Training MAE 2: {:0.4}".format(min_mae_2))
    print("Training MAE 3: {:0.4}".format(min_mae_3))
    print("Validation MSE loss: {:0.4}".format(min_val_loss))
    print("Validation MAE 1: {:0.4}".format(min_val_mae_1))
    print("Validation MAE 2: {:0.4}".format(min_val_mae_2))
    print("Validation MAE 3: {:0.4}".format(min_val_mae_3))

    # Plot learning curves
    loss = history.history['loss']
    val_loss = history.history['val_loss']
    mae_1 = history.history['output_1_mean_absolute_error']
    val_mae_1 = history.history['val_output_1_mean_absolute_error']
    mae_2 = history.history['output_2_mean_absolute_error']
    val_mae_2 = history.history['val_output_2_mean_absolute_error']
    mae_3 = history.history['output_3_mean_absolute_error']
    val_mae_3 = history.history['val_output_3_mean_absolute_error']

    stopped_epoch = len(loss)
    epochs_used = range(1, stopped_epoch + 1)

    plt.plot(epochs_used, loss, 'b', label="Training mse")
    plt.plot(epochs_used, val_loss, 'r', label="Validation mse")
    plt.title("Training and validation mse")
    plt.legend()
    plt.savefig(m.model_dir/"mse_loss.png")

    plt.figure()
    plt.plot(epochs_used, mae_1, 'b', label="Training mae")
    plt.plot(epochs_used, val_mae_1, 'r', label="Validation mae")
    plt.title("Training and validation MAE (Output 1)")
    plt.legend()
    plt.savefig(m.model_dir/"mae_metric_1.png")

    plt.figure()
    plt.plot(epochs_used, mae_2, 'b', label="Training mae")
    plt.plot(epochs_used, val_mae_2, 'r', label="Validation mae")
    plt.title("Training and validation MAE (Output 2)")
    plt.legend()
    plt.savefig(m.model_dir/"mae_metric_2.png")

    plt.figure()
    plt.plot(epochs_used, mae_3, 'b', label="Training mae")
    plt.plot(epochs_used, val_mae_3, 'r', label="Validation mae")
    plt.title("Training and validation MAE (Output 3)")
    plt.legend()
    plt.savefig(m.model_dir/"mae_metric_3.png")

    plt.show()

    # Save training meta data to file
    m.save(m.model_dir / "meta.pkl")


if __name__ == '__main__':
    main()
