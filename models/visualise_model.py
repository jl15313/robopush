import os
import datetime
import re
from pathlib import Path

import numpy as np
from matplotlib import pyplot as plt
import cv2

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()

from tensorflow.keras import models
from tensorflow.compat.v1.keras import backend as K

from robopush.utils import Namespace


# Helper function for de-normalising image
def deprocess_image(x):
    x -= x.mean()
    x /= (x.std() + 1e-5)
    x *= 0.1

    x += 0.5
    x = np.clip(x, 0, 1)

    x *= 255
    x = np.clip(x, 0, 255).astype('uint8')
    return x

# Helper function for generating an input pattern that maximises the
# nth filter of the specified layer
def generate_pattern(model, layer_name, filter_index, size=128):
    # Build a loss function that maximizes the activation
    # of the nth filter of the specified layer
    layer_output = model.get_layer(layer_name).output
    loss = K.mean(layer_output[:, :, :, filter_index])

    # Compute the gradient of the input wrt this loss
    grads = K.gradients(loss, model.input)[0]

    # Normalization trick: we normalize the gradient
    grads /= (K.sqrt(K.mean(K.square(grads))) + 1e-5)

    # This function returns the loss and grads given the input
    iterate = K.function([model.input], [loss, grads])

    # We start from a gray image with some noise
    input_img_data = np.random.random((1, size, size, 1)) * 20 + 128.

    # Run gradient ascent for 40 steps
    step = 1.
    for i in range(40):
        loss_value, grads_value = iterate([input_img_data])
        input_img_data += grads_value * step

    img = input_img_data[0]
    return deprocess_image(img)

def main():
    m = Namespace()

    # Data dirs/files
    m.root_dir = Path(os.environ["DATAPATH"])
    m.train_data_dir = m.root_dir/"dynamics/data/surface3d_sliding/data_02031056"
    m.test_data_dir = m.root_dir/"dynamics/data/surface3d_sliding/data_02031220"
    m.model_dir = m.train_data_dir/"models/mdl_02031604"
    m.analysis_dir = m.test_data_dir/("analysis/test3d_" + datetime.datetime.now().strftime("%m%d%H%M"))
    m.test_image_dir = m.test_data_dir/"images"
    m.test_pp_image_dir = m.test_image_dir/"pp_images"
    m.test_image_file = "image_1251_1.jpg"

    # Sensor image pre-processing params
    m.crop_x, m.crop_y = 115, 25
    m.crop_w, m.crop_h = 430, 430
    m.median_blur_kernel_size = 5
    m.adapt_thresh_block_size = 11
    m.adapt_thresh_offset = -5
    m.resize_w, m.resize_h = 128, 128

    # CNN params
    m.input_dims = (128, 128)

    # Visualisation params
    m.layer_name_pattern_string = "activation_[1-6]+"
    m.images_per_row = 16

    # Helper function/closure for pre-processing sensor image
    def preprocess(image):
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        image = image[m.crop_y:m.crop_y+m.crop_h, m.crop_x:m.crop_x+m.crop_w]
        image = cv2.medianBlur(image, m.median_blur_kernel_size)
        image = cv2.adaptiveThreshold(image.astype('uint8'), 255, \
                                      cv2.ADAPTIVE_THRESH_GAUSSIAN_C, \
                                      cv2.THRESH_BINARY, \
                                      m.adapt_thresh_block_size, m.adapt_thresh_offset)
        image = cv2.resize(image, (m.resize_w, m.resize_h), interpolation=cv2.INTER_AREA)
        return np.expand_dims(image, axis=-1)

    # Helper function/closure for pre-processing all sensor images in dir
    def preprocess_dir(src_dir, dest_dir, suffix='.jpg'):
        file_names = (entry for entry in os.listdir(src_dir) if entry.endswith(suffix))
        for file_name in file_names:
            image = cv2.imread(str(src_dir/file_name))
            image = preprocess(image)
            cv2.imwrite(str(dest_dir/file_name), image)

    # Batch pre-process all test images
    if not os.path.isdir(m.test_pp_image_dir):
        os.makedirs(m.test_pp_image_dir)
        preprocess_dir(m.test_image_dir, m.test_pp_image_dir)

    # Create missing dirs
    if not os.path.exists(m.analysis_dir):
        os.makedirs(m.analysis_dir)

    # Configure GPU
    config = tf.ConfigProto(
        gpu_options=tf.GPUOptions(per_process_gpu_memory_fraction=0.8)
        # device_count = {'GPU': 1}
    )
    config.gpu_options.allow_growth = True
    session = tf.Session(config=config)
    K.set_session(session)
    K.clear_session()

    # Load test image
    test_image = cv2.imread(str(m.test_pp_image_dir/m.test_image_file), cv2.IMREAD_GRAYSCALE)
    test_image = np.expand_dims(test_image, axis=(0, -1))
    plt.imshow(test_image.squeeze(), cmap='gray')
    plt.show()

    # Load CNN model
    model = models.load_model(m.model_dir/"best_model.h5")
    model.summary()

    # Create relevant hooks into CNN model
    layer_name_pattern = re.compile(m.layer_name_pattern_string)
    layers = [layer for layer in model.layers if layer_name_pattern.match(layer.name)]
    layer_names = [layer.name for layer in layers]
    layer_outputs = [layer.output for layer in layers]
    activation_model = models.Model(inputs=model.input, outputs=layer_outputs)
    activations = activation_model.predict(test_image)

    # Visualise CNN filter activations for test image
    print("Visualising CNN activations for test image ...")
    for layer_name, layer_activation in zip(layer_names, activations):
        n_features = layer_activation.shape[-1]
        size = layer_activation.shape[1]
        n_rows = n_features // m.images_per_row
        display_grid = np.zeros((n_rows * size, m.images_per_row * size))

        for row in range(n_rows):
            for col in range(m.images_per_row):
                import pdb

                if len(layer_activation.shape) != 4: pdb.set_trace()
                channel_image = layer_activation[0,:,:,row * m.images_per_row + col]
                channel_image -= channel_image.mean()
                channel_image_std = channel_image.std()
                if channel_image_std > 0:
                    channel_image /= channel_image_std
                channel_image *= 64
                channel_image += 128
                channel_image = np.clip(channel_image, 0, 255).astype('uint8')
                display_grid[row * size: (row + 1) * size,
                col * size: (col + 1) * size] = channel_image

        scale = 1./size
        plt.figure(figsize=(scale * display_grid.shape[1],
                            scale * display_grid.shape[0]))
        plt.title(layer_name)
        plt.grid(False)
        plt.imshow(display_grid, aspect='auto', cmap='gray')
        plt.savefig(m.analysis_dir/("response_" + layer_name + ".png"))

    plt.show()

    # Visualise input pattern that maximally activates CNN filters
    print("Visualising input that maximally activates CNN filters (this may take some time) ...")
    for layer_name in layer_names:
        n_features = model.get_layer(layer_name).output.shape[-1]
        n_rows = n_features // m.images_per_row
        n_cols = m.images_per_row
        size = np.max(m.input_dims)
        margin = 5

        results = np.zeros((n_rows * size + (n_rows - 1) * margin,
                            n_cols * size + (n_cols - 1) * margin, 1))
        for i in range(n_rows):
            for j in range(n_cols):
                filter_img = generate_pattern(model, layer_name, (i * n_cols) + j, size=size)
                vertical_start = i * size + i * margin
                vertical_end = vertical_start + size
                horizontal_start = j * size + j * margin
                horizontal_end = horizontal_start + size
                results[vertical_start:vertical_end, horizontal_start:horizontal_end, :] = filter_img

        plt.figure(figsize=(20, 20))
        plt.title(layer_name)
        plt.imshow(np.squeeze(results), cmap='gray')
        plt.savefig(os.path.join(m.analysis_dir, "filter_" + layer_name + ".png"))

    plt.show()

    print("Finished!")

    # Save testing meta data to file
    m.save(m.analysis_dir/"meta.pkl")


if __name__ == '__main__':
    main()
