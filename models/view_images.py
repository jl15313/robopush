import os
from pathlib import Path

import numpy as np
from matplotlib import pyplot as plt
import cv2

from robopush.utils import Namespace


def main():
    m = Namespace()

    # Data dirs
    root_dir = Path(os.environ["DATAPATH"])
    data_dir = root_dir/"dynamics/data/surface3d_sliding/data_09080806"
    image_dir = data_dir/"images"
    image_file = image_dir/"image_1672_1.jpg"

    # Sensor image pre-processing params
    m.crop_x, m.crop_y = 115, 25
    m.crop_w, m.crop_h = 430, 430
    m.median_blur_kernel_size = 5
    m.adapt_thresh_block_size = 11
    m.adapt_thresh_offset = -5
    m.resize_w, m.resize_h = 128, 128

    # Helper function/closure for pre-processing sensor image
    def preprocess(image):
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        image = image[m.crop_y:m.crop_y+m.crop_h, m.crop_x:m.crop_x+m.crop_w]
        image = cv2.medianBlur(image, m.median_blur_kernel_size)
        image = cv2.adaptiveThreshold(image.astype('uint8'), 255, \
                                      cv2.ADAPTIVE_THRESH_GAUSSIAN_C, \
                                      cv2.THRESH_BINARY, \
                                      m.adapt_thresh_block_size, m.adapt_thresh_offset)
        image = cv2.resize(image, (m.resize_w, m.resize_h), interpolation=cv2.INTER_AREA)
        return np.expand_dims(image, axis=-1)

    # Grab an image from file and display it
    image = cv2.imread(str(image_file))
    plt.figure()
    plt.imshow(image.squeeze(), cmap='gray')

    # Pre-process the image and display it
    pp_image = preprocess(image)
    pp_image = np.expand_dims(pp_image, axis=0)
    plt.figure()
    plt.imshow(pp_image.squeeze(), cmap='gray')

    plt.show()


if __name__ == '__main__':
    main()