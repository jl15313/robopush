import os
from pathlib import Path

import pandas as pd

from robopush.utils import Namespace


def main():
    # Data dirs
    root_dir = Path(os.environ["DATAPATH"])
    expt_dir = root_dir/"dynamics/test/multi_test"

    # Expt 1
    # data = pd.DataFrame(columns=['parent_dir', 'child_dir', 'p_y', 'p_z', 'rho', 'theta'])

    # Expt 2-5
    df = pd.DataFrame(columns=['parent_dir', 'child_dir', 'p_y', 'p_z', 'rho', 'theta'])

    for dir in expt_dir.iterdir():
        if not dir.is_dir():
            continue

        parent_dir = dir.stem
        print(f"{parent_dir}")

        # Iterate over valid shape dirs ...
        for subdir in dir.iterdir():
            if not subdir.is_dir():
                continue
            if not subdir.stem.startswith("test_"):
                continue

            child_dir = subdir.stem
            print(f"\t{child_dir}")

            # Load experiment meta data
            m = Namespace()
            m.load(subdir/"meta.pkl")

            # Expt 1
            # df = df.append({'parent_dir': parent_dir,
            #                 'child_dir': child_dir,
            #                 'p_y': m.p_y[-1],
            #                 'p_z': m.p_z[-1],
            #                 'rho': m.rho[-1],
            #                 'theta': m.theta[-1],
            #                 }, ignore_index=True)

            # Expt 2-5
            df = df.append({'parent_dir': parent_dir,
                            'child_dir': child_dir,
                            'p_y': m.p_y[-1],
                            'p_z': m.p_z[-1],
                            'rho': m.rho[-1],
                            'theta': m.theta[-1],
                            }, ignore_index=True)

    df.to_csv(os.path.join(expt_dir, "data.csv"), index=False)


if __name__ == '__main__':
    main()
