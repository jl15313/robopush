import os
from pathlib import Path

import cv2

from robopush.utils import Namespace


def main():
    # Data dirs
    root_dir = Path(os.environ["DATAPATH"])
    expt_dir = root_dir/"dynamics/test/multi_test"

    fourcc = cv2.VideoWriter_fourcc(*'mp4v')

    print("Combining images to create video ...")
    for dir in expt_dir.iterdir():
        if not dir.is_dir():
            continue

        # Iterate over valid shape dirs ...
        for subdir in dir.iterdir():
            if not subdir.is_dir():
                continue
            if not subdir.stem.startswith("test_"):
                continue
            print(f"\t{subdir.stem}")

            # Load experiment meta data
            m = Namespace()
            m.load(subdir/"meta.pkl")

            frame_size = m.color_size
            fps = m.color_fps

            camera_images_dir = subdir/"camera_images"
            analysis_dir = subdir/"analysis"
            video_file = analysis_dir/"camera_images.mp4"

            # Create missing dirs
            if not os.path.exists(analysis_dir):
                os.makedirs(analysis_dir)

            # Initialise video writer
            writer = cv2.VideoWriter(str(video_file), fourcc, fps=fps,
                                     frameSize=frame_size, isColor=True)

            try:
                files = os.listdir(camera_images_dir)
                file_extensions = ['jpg', 'jpeg', 'bmp', 'png', 'gif']
                files = [f for f in files if any(f.endswith(e) for e in file_extensions)]
                files = sorted(files, key=lambda f: int(''.join(filter(str.isdigit, f))))
                for file in files:
                    path = camera_images_dir / file
                    frame = cv2.imread(str(path))
                    if frame.shape[1::-1] != frame_size:
                        frame = cv2.resize(frame, frame_size)
                    writer.write(frame)
            finally:
                writer.release()

    print("Finished!")


if __name__ == '__main__':
    main()