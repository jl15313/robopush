import os
from pathlib import Path

import numpy as np
from matplotlib import pyplot as plt

from cri.transforms import mat2euler

from robopush.utils import Namespace, inv_transform_euler
from analysis.patches import circles, polygons
from analysis.plot import SENSOR_POLYGON, IRREGULAR_OBJ_POLYGONS, IRREGULAR_OBJ_RADII


def main():
    # Data dirs
    root_dir = Path(os.environ["DATAPATH"])
    expt_dir = root_dir/"dynamics/test/windex/test_09291355"

    # Plot params
    obj_shape = 'windex'
    plot_interval = 5

    # Load experiment meta data
    m = Namespace()
    m.load(expt_dir/"meta.pkl")

    # Convert object poses (wrt base frame) from 4x4 homogeneous matrices to Euler format
    base_obj_poses = np.array([mat2euler(p, axes=m.robot_axes) for p in m.base_poses])

    # Compute sensor poses wrt base frame
    base_sensor_poses = np.array([inv_transform_euler(p, m.work_frame, m.robot_axes) for p in m.work_align])

    # Compute equivalent z-axis rotation for base frame pose that includes a -90 degree extrinsic
    # rotation around y-axis
    base_sensor_poses[:, 3] -= 180
    base_sensor_poses[:, 3] += base_sensor_poses[:, 5]
    base_sensor_poses[:, 4:6] = 0

    # Assemble plot data for every nth step
    base_object_poses_to_plot = np.vstack((m.base_centroids[::plot_interval, 0],
                                           m.base_centroids[::plot_interval, 1],
                                           base_obj_poses[::plot_interval, 5])).T
    base_sensor_poses_to_plot = np.vstack((base_sensor_poses[::plot_interval, 0],
                                           base_sensor_poses[::plot_interval, 1],
                                           base_sensor_poses[::plot_interval, 3])).T

    # Plot object poses
    plt.figure()
    for i in range(len(base_object_poses_to_plot)):
        if obj_shape in IRREGULAR_OBJ_RADII.keys():
            circles(x=base_object_poses_to_plot[i, 0] + IRREGULAR_OBJ_RADII[obj_shape][0],
                    y=base_object_poses_to_plot[i, 1] + IRREGULAR_OBJ_RADII[obj_shape][1],
                    s=IRREGULAR_OBJ_RADII[obj_shape][2],
                    alpha=0.7, fc='0.9', ec='k', lw=1.5)
        if obj_shape in IRREGULAR_OBJ_POLYGONS.keys():
            for j in range(len(IRREGULAR_OBJ_POLYGONS[obj_shape])):
                polygons(x=base_object_poses_to_plot[i, 0], y=base_object_poses_to_plot[i, 1],
                         v=IRREGULAR_OBJ_POLYGONS[obj_shape][j],
                         rot=base_object_poses_to_plot[i, 2],
                         alpha=0.7, fc='0.9', ec='k', lw=1.5)

    # Plot sensor poses
    polygons(x=base_sensor_poses_to_plot[:, 0], y=base_sensor_poses_to_plot[:, 1],
             v=SENSOR_POLYGON, rot=base_sensor_poses_to_plot[:, 2],
             alpha=0.7, fc='0.9', ec='#d62728', lw=1.5)

    # Plot target
    circles(x=m.base_target_pose[0], y=m.base_target_pose[1],
            s=12, alpha=0.7, fc='#2ca02c', ec='none')

    plt.xlabel("x (mm)")
    plt.ylabel("y (mm)")
    plt.xlim(-135, 445)
    plt.ylim(-680, -260)
    plt.gca().set_aspect('equal', adjustable='box')
    plt.gca().axes.get_xaxis().set_visible(False)
    plt.gca().axes.get_yaxis().set_visible(False)
    plt.draw()
    plt.show()

    # plt.savefig("test.svg", format='svg', bbox_inches='tight')


if __name__ == '__main__':
    main()