import os
from pathlib import Path

import cv2

from robopush.utils import Namespace


def main():
    # Data dirs/files
    root_dir = Path(os.environ["DATAPATH"])
    expt_dir = root_dir/"dynamics/test/test_01271708"
    camera_images_dir = expt_dir/"camera_images"
    analysis_dir = expt_dir/"analysis"
    video_file = analysis_dir/"camera_images.mp4"

    # Load experiment meta data
    m = Namespace()
    m.load(expt_dir/"meta.pkl")

    frame_size = m.color_size
    fps = m.color_fps

    # Create missing dirs
    if not os.path.exists(analysis_dir):
        os.makedirs(analysis_dir)

    # Initialise video writer
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    writer = cv2.VideoWriter(str(video_file), fourcc=fourcc, fps=fps,
                             frameSize=frame_size, isColor=True)
    try:
        print("Combining images to create video ...")
        files = os.listdir(camera_images_dir)
        file_extensions = ['jpg', 'jpeg', 'bmp', 'png', 'gif']
        files = [f for f in files if any(f.endswith(e) for e in file_extensions)]
        files = sorted(files, key=lambda f: int(''.join(filter(str.isdigit, f))))
        for file in files:
            path = camera_images_dir/file
            frame = cv2.imread(str(path))
            if frame.shape[1::-1] != frame_size:
                frame = cv2.resize(frame, frame_size)
            writer.write(frame)
    finally:
        writer.release()

    print("Finished!")


if __name__ == '__main__':
    main()