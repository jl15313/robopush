import os
from pathlib import Path

import numpy as np
from matplotlib import pyplot as plt

from cri.transforms import mat2euler

from robopush.utils import Namespace, inv_transform_euler
from analysis.patches import circles, polygons
from analysis.plot import REGULAR_OBJ_POLYGONS, SHAPE_MAP


def main():
    # Data dirs
    root_dir = Path(os.environ["DATAPATH"])
    expt_dir = root_dir/"dynamics/test/multi_test"

    # Plot params
    plot_interval = 5

    # Use ggplot plot colors
    plt.style.use('ggplot')
    prop_cycle = plt.rcParams['axes.prop_cycle']
    plt.style.use('default')
    plt.rcParams['axes.prop_cycle'] = prop_cycle

    plt.figure(1)
    plt.figure(2)

    # Iterate over dirs in experiment dir ...
    for dir in expt_dir.iterdir():
        if not dir.is_dir():
            continue

        # Check that dir name contains valid shape key
        dir_shape = None
        for shape in SHAPE_MAP.keys():
            if shape in dir.stem:
                dir_shape = shape
                break
        if dir_shape is None:
            continue
        print(dir)

        # Iterate over valid shape dirs ...
        for subdir in dir.iterdir():
            if not subdir.is_dir():
                continue
            if not subdir.stem.startswith("test_"):
                continue
            print(f"\t{subdir.stem}")

            # Map subdir name to object shape
            obj_shape = SHAPE_MAP[dir_shape]

            # Load experiment meta data
            m = Namespace()
            m.load(subdir/"meta.pkl")

            # Convert object poses (wrt base frame) from 4x4 homogeneous matrices to Euler format
            base_obj_poses = np.array([mat2euler(p, axes=m.robot_axes) for p in m.base_poses])

            # Compute sensor poses wrt base frame
            base_sensor_poses = np.array(
                [inv_transform_euler(p, m.work_frame, m.robot_axes) for p in m.work_align])

            # Compute equivalent z-axis rotation for base frame pose that includes a -90 degree extrinsic
            # rotation around y-axis
            base_sensor_poses[:, 3] -= 180
            base_sensor_poses[:, 3] += base_sensor_poses[:, 5]
            base_sensor_poses[:, 4:6] = 0

            # Compute sensor tip poses wrt base frame
            base_tip_poses = np.array(
                [inv_transform_euler([0, 0, 20, 0, 0, 0], p, m.robot_axes) for p in base_sensor_poses])

            # Assemble plot data for every nth step
            base_object_poses_to_plot = np.vstack((m.base_centroids[::plot_interval, 0],
                                                   m.base_centroids[::plot_interval, 1],
                                                   base_obj_poses[::plot_interval, 5])).T
            base_tip_poses_to_plot = np.vstack((base_tip_poses[::plot_interval, 0],
                                                base_tip_poses[::plot_interval, 1],
                                                base_tip_poses[::plot_interval, 3])).T

            # Plot object tracks
            plt.figure(1)
            plt.plot(base_object_poses_to_plot[:, 0], base_object_poses_to_plot[:, 1], alpha=0.7, lw=1)

            # Plot sensor tracks
            plt.figure(2)
            plt.plot(base_tip_poses_to_plot[:, 0], base_tip_poses_to_plot[:, 1], alpha=0.7, lw=1)

    # Object tracks figure ...
    plt.figure(1)

    # Plot approximate start and finish positions of object (blue square)
    polygons(x=-22.5, y=-327.5, v=REGULAR_OBJ_POLYGONS[obj_shape],
             rot=0.0, alpha=0.7, fc='0.9', ec='k', lw=1, ls=':', zorder=-10)
    polygons(x=352.0, y=-550.0, v=REGULAR_OBJ_POLYGONS[obj_shape],
             rot=55.0, alpha=0.7, fc='0.9', ec='k', lw=1, ls=':', zorder=-10)

    # Plot target
    circles(x=m.base_target_pose[0], y=m.base_target_pose[1], s=12,
            alpha=0.7, fc='#2ca02c', ec='none', zorder=-10)

    # plt.figtext(0.65, 0.7, "${y_{targ} = " + "{0:.2f}".format(m.p_y[-1]) + "}$", fontsize=12)

    # plt.title("Object tracks")
    plt.xlabel("x (mm)")
    plt.ylabel("y (mm)")
    plt.xlim(-135, 425)
    plt.ylim(-630, -260)
    plt.gca().set_aspect('equal', adjustable='box')
    plt.gca().axes.get_xaxis().set_visible(False)
    plt.gca().axes.get_yaxis().set_visible(False)
    plt.draw()

    # plt.savefig(expt_dir/"object_tracks.svg", format="svg", bbox_inches='tight')

    # Sensor tracks figure ...
    plt.figure(2)

    # Plot approximate start and finish positions of object (blue square)
    polygons(x=-22.5, y=-322.5, v=REGULAR_OBJ_POLYGONS['blue_square'],
             rot=0.0, alpha=0.7, fc='0.9', ec='k', lw=1, ls=':', zorder=-10)
    polygons(x=352.0, y=-550.0, v=REGULAR_OBJ_POLYGONS['blue_square'],
             rot=55.0, alpha=0.7, fc='0.9', ec='k', lw=1, ls=':', zorder=-10)

    # Plot target
    circles(x=m.base_target_pose[0], y=m.base_target_pose[1], s=12, alpha=0.7, fc='#2ca02c', ec='none')

    # plt.figtext(0.65, 0.7, "${y_{targ} = " + "{0:.2f}".format(m.p_y[-1]) + "}$", fontsize=12)

    # plt.title("Sensor tracks")
    plt.xlabel("x (mm)")
    plt.ylabel("y (mm)")
    plt.xlim(-135, 425)
    plt.ylim(-630, -260)
    plt.gca().set_aspect('equal', adjustable='box')
    plt.gca().axes.get_xaxis().set_visible(False)
    plt.gca().axes.get_yaxis().set_visible(False)
    plt.draw()

    # plt.savefig(expt_dir/"sensor_tracks.svg", format="svg", bbox_inches='tight')

    plt.show()


if __name__ == '__main__':
    main()
