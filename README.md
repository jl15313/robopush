# robopush

Experimental code used in the paper ["Goal-Driven Robotic Pushing Using Tactile and Proprioceptive Feedback"](https://arxiv.org/abs/2012.01859).