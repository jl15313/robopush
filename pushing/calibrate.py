import os
import time
from pathlib import Path

import numpy as np
import cv2

from cri.robot import SyncRobot
from cri.controller import RTDEController

from robopush.camera import RSCamera, DistanceError
from robopush.detector import ArUcoDetector
from robopush.utils import Namespace, inv_transform_euler


def main():
    np.random.seed(0)

    # Calibration meta data
    m = Namespace()

    # Data dirs
    m.root_dir = Path(os.environ["DATAPATH"])
    m.calib_dir = m.root_dir/("dynamics/calib/calib_" + time.strftime('%m%d%H%M'))
    m.image_dir = m.calib_dir/"images"

    # Robot params
    m.robot_ip = '192.11.72.10'
    m.robot_axes = 'sxyz'
    m.robot_tcp = [41.01, -41.01, 80, -90, 90, -135]
    m.base_frame = [0, 0, 0, 0, 0, 0]
    m.base_home_pose = [107.4, -547.1, 350.4, 90, -90, 0]
    m.work_frame = [-103.7, -427.5, 100.0, 90, -90, 0]
    m.work_start_pose = [0, 0, 0, 0, 0, 0]

    # Ranges for random offsets wrt start pose in work frame
    m.x_rng = [0, 30]
    m.y_rng = [0, -500]
    m.z_rng = [-100, 150]
    m.alpha_rng = [0, 0]
    m.beta_rng = [30, 30]
    m.gamma_rng = [0, 0]

    m.n_samples = 50

    # RealSense camera and ArUco detector params
    m.color_size = (1920, 1080)
    m.color_fps = 30
    m.depth_size = (848, 480)
    m.depth_fps = 30
    m.marker_length = 25.0
    m.dict_id = cv2.aruco.DICT_7X7_50

    # Create missing dirs
    if not os.path.exists(m.calib_dir):
        os.makedirs(m.calib_dir)
    if not os.path.exists(m.image_dir):
        os.makedirs(m.image_dir)

    # Initialise robot, camera and detector
    robot = SyncRobot(RTDEController(ip=m.robot_ip))
    camera = RSCamera(color_size=m.color_size, color_fps=m.color_fps,
                      depth_size=m.depth_size, depth_fps=m.depth_fps)
    detector = ArUcoDetector(camera, marker_length=m.marker_length, dict_id=m.dict_id)

    try:
        # Configure robot
        robot.axes = m.robot_axes
        robot.tcp = m.robot_tcp
        robot.linear_speed = 100

        # Move to home position wrt base frame
        print("Moving to home position ...")
        robot.coord_frame = m.base_frame
        robot.move_linear(m.base_home_pose)

        # Move to start position wrt work frame
        print("Moving to start position ...")
        robot.coord_frame = m.work_frame
        robot.move_linear(m.work_start_pose)

        # Initialise calibration data
        print("Detecting marker poses ...")
        m.work_poses, m.base_poses, m.centroids, m.centroid_positions, \
            m.rvecs, m.tvecs = [], [], [], [], [], []

        n_poses, n_detected = 0, 0
        while n_detected < m.n_samples:
            # Sample random pose from range (start pose + offset)
            work_pose = m.work_start_pose + np.random.uniform(low=(m.x_rng[0],
                                                                   m.y_rng[0],
                                                                   m.z_rng[0],
                                                                   m.alpha_rng[0],
                                                                   m.beta_rng[0],
                                                                   m.gamma_rng[0]),
                                                              high=(m.x_rng[1],
                                                                    m.y_rng[1],
                                                                    m.z_rng[1],
                                                                    m.alpha_rng[1],
                                                                    m.beta_rng[1],
                                                                    m.gamma_rng[1]))

            base_pose = inv_transform_euler(work_pose, m.work_frame)

            # Move to sampled pose
            robot.move_linear(work_pose)

            # Detect ArUco marker in RealSense camera frame
            n_poses += 1
            try:
                detector.detect()
            except DistanceError:
                print(f"pose {n_poses}: marker distance error - moving to next pose")
                continue
            if detector.ids is None:
                print(f"pose {n_poses}: marker not detected - moving to next pose")
                continue
            if len(detector.ids) > 1:
                print(f"pose {n_poses}: multiple markers detected - moving to next pose")
                continue
            print(f"pose {n_poses}: marker detected - moving to next pose")
            n_detected += 1

            # Display ArUco marker
            marker_image = detector.draw_markers(camera.color_image)
            cv2.imwrite(str(m.image_dir/("image_" + str(n_detected) + ".jpg")), marker_image)
            cv2.imshow("Detected marker", marker_image)
            cv2.waitKey(1)

            # Capture calibration data for sampled pose
            m.work_poses.append(work_pose)
            m.base_poses.append(base_pose)
            m.centroids.append(detector.centroids[0])
            m.centroid_positions.append(detector.centroid_positions[0])
            m.rvecs.append(detector.rvecs[0])
            m.tvecs.append(detector.tvecs[0])

        # Convert calibration data to numpy arrays
        m.work_poses = np.array(m.work_poses)
        m.base_poses = np.array(m.base_poses)
        m.centroids = np.array(m.centroids)
        m.centroid_positions = np.array(m.centroid_positions)
        m.rvecs = np.array(m.rvecs)
        m.tvecs = np.array(m.tvecs)

        # Move to start position wrt work frame
        print("Moving to start position ...")
        robot.move_linear(m.work_start_pose)

        # Move to home position wrt base frame
        print("Moving to home position ...")
        robot.coord_frame = m.base_frame
        robot.move_linear(m.base_home_pose)

    finally:
        # Clean up
        robot.close()
        camera.close()

        # Save calibration meta data to file
        m.save(m.calib_dir / "meta.pkl")

    # Solve point correspondences for extrinsic camera params and save to file
    ret, rvec, tvec = cv2.solvePnP(np.ascontiguousarray(m.base_poses[:, 0:3]), m.centroids, \
                                   camera.color_cam_matrix, camera.color_dist_coeffs)

    with np.printoptions(precision=2, suppress=True):
        print(f"ret: {ret}, rvec: {rvec}, tvec: {tvec}")

    if ret:
        # Save extrinsic params to file
        ext = Namespace()
        ext.ret = ret
        ext.rvec = rvec
        ext.tvec = tvec
        ext.save(m.calib_dir/"extrinsics.pkl")
    else:
        print("Failed to solve point correspondence - extrinsic parameters not saved")


if __name__ == '__main__':
    main()
