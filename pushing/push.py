import os
import time
from pathlib import Path

import numpy as np
from matplotlib import pyplot as plt
import cv2

import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
from tensorflow.keras import models

from cri.robot import SyncRobot, AsyncRobot
from cri.controller import RTDEController

from vsp.video_stream import CvVideoCamera, CvVideoDisplay, CvImageOutputFileSeq
from vsp.processor import CameraStreamProcessorMT, AsyncProcessor

from robopush.controller import PIDController
from robopush.camera import RSCamera, DistanceError, ColorFrameError, DepthFrameError
from robopush.detector import ArUcoDetector
from robopush.tracker import ArUcoTracker, NoMarkersDetected, MultipleMarkersDetected
from robopush.utils import Namespace, transform_euler, inv_transform_euler


def main():
    ### EXPERIMENT PARAMETERS / META DATA

    m = Namespace()

    # Data dirs
    m.root_dir = Path(os.environ["DATAPATH"])
    m.train_data_dir = m.root_dir/"dynamics/data/surface3d_sliding/data_09080806"
    m.model_dir = m.train_data_dir/"models/mdl_09091537"
    m.expt_dir = m.root_dir/("dynamics/test/test_" + time.strftime('%m%d%H%M'))
    m.extrinsics_dir = m.root_dir/"dynamics/calib/calib_01271107"
    m.sensor_image_dir = m.expt_dir/"sensor_images"
    m.camera_image_dir = m.expt_dir/"camera_images"
    m.figure_dir = m.expt_dir/"figures"

    # TacTip params
    m.sensor_source = 1
    m.sensor_exposure = -6

    # RealSense camera, ArUco detector and tracker params
    m.color_size = (1920, 1080)
    m.color_fps = 30
    m.depth_size = (848, 480)
    m.depth_fps = 30
    m.marker_length = 25.0
    m.dict_id = cv2.aruco.DICT_7X7_50
    m.track_attempts = 100

    # Sensor image pre-processing params
    m.crop_x, m.crop_y = 115, 25
    m.crop_w, m.crop_h = 430, 430
    m.median_blur_kernel_size = 5
    m.adapt_thresh_block_size = 11
    m.adapt_thresh_offset = -5
    m.resize_w, m.resize_h = 128, 128

    # CNN params
    m.input_dims = (128, 128)
    m.input_rescale = 1./255.

    # Robot params
    m.robot_ip = '192.11.72.10'
    m.robot_axes = 'sxyz'
    m.robot_tcp = [42.43, -42.43, 80, -90, 90, -135]

    # Coord frames and motion params
    m.base_frame = [0, 0, 0, 0, 0, 0]
    m.base_home_pose = [107.4, -547.1, 350.4, 45, -90, 45]
    m.work_frame = [-83.7, -327.5, 70.0, 180, -90, 0]
    m.work_target_pose = [0, 200, 400, 0, 0, 0]
    m.sensor_tap_move = [[0.0, 0.0, 10.0, 0.0, 0.0, 0.0],
                         [0.0, 0.0, 5.0, 0.0, 0.0, 0.0]]
    m.sensor_tip_radius = 20.0
    m.max_push_steps = 250

    # Sensor/pusher start poses for different experiments ...

    # Start poses for IRREGULAR objects
    # m.work_start_pose = [-10, 0, 0, 0, 0, 0]          # start 1 / blue foam
    m.work_start_pose = [-10, 0, 200, -150, 0, 0]       # start 2 / blue foam
    # m.work_start_pose = [-20, 0, 0, 0, 0, 0]          # start 1 / MDF surface

    # Start poses for REGULAR objects on CONCAVE surface
    # m.work_start_pose = [35, 0, 0, 0, 0, 0]           # start 1
    # m.work_start_pose = [10, 0, 200, -150, 0, 0]      # start 2
    # m.work_start_pose = [10, 200, 150, 45, 0, 0]      # start 3

    # Start poses for REGULAR objects on CONVEX surface
    # m.work_start_pose = [20, 0, 0, 0, 0, 0] #         # start 1
    # m.work_start_pose = [45, 0, 200, -150, 0, 0]      # start 2
    # m.work_start_pose = [45, 200, 150, 45, 0, 0]      # start 3

    # Start poses for REGULAR objects on FLAT surface
    # m.work_start_pose = [0, 0, 0, 0, 0, 0]            # start 1
    # m.work_start_pose = [0, 0, 200, -150, 0, 0]       # start 2
    # m.work_start_pose = [0, 200, 150, 45, 0, 0]       # start 3

    # Start poses for blue square/cube on FLAT surface (different spatial/angular starts)
    # m.work_start_pose = [0, 0, 0, 0, 0, 0]            # offset 00 / angle 00
    # m.work_start_pose = [0, 0, 0, 20, 0, 0]           # offset 00 / angle 20
    # m.work_start_pose = [0, 0, 0, -20, 0, 0]          # offset 00 / angle -20
    # m.work_start_pose = [0, 10, 0, 0, 0, 0]           # offset 20 / angle 00
    # m.work_start_pose = [0, 10, 0, 20, 0, 0]          # offset 20 / angle 20
    # m.work_start_pose = [0, 10, 0, -20, 0, 0]         # offset 20 / angle -20
    # m.work_start_pose = [0, -10, 0, 0, 0, 0]          # offset 20 / angle 00
    # m.work_start_pose = [0, -10, 0, 20, 0, 0]         # offset 20 / angle 20
    # m.work_start_pose = [0, -10, 0, -20, 0, 0]        # offset 20 / angle -20
    # m.work_start_pose = [0, 20, 0, 0, 0, 0]           # offset 20 / angle 00
    # m.work_start_pose = [0, 20, 0, 20, 0, 0]          # offset 20 / angle 20
    # m.work_start_pose = [0, 20, 0, -20, 0, 0]         # offset 20 / angle -20
    # m.work_start_pose = [0, -20, 0, 0, 0, 0]          # offset -20 / angle 00
    # m.work_start_pose = [0, -20, 0, 20, 0, 0]         # offset -20 / angle 20
    # m.work_start_pose = [0, -20, 0, -20, 0, 0]        # offset -20 / angle -20
    # m.work_start_pose = [0, 30, 0, 0, 0, 0]           # offset -20 / angle 00
    # m.work_start_pose = [0, 30, 0, 20, 0, 0]          # offset -20 / angle 20
    # m.work_start_pose = [0, 30, 0, -20, 0, 0]         # offset -20 / angle -20
    # m.work_start_pose = [0, -30, 0, 0, 0, 0]          # offset -20 / angle 00
    # m.work_start_pose = [0, -30, 0, 20, 0, 0]         # offset -20 / angle 20
    # m.work_start_pose = [0, -30, 0, -20, 0, 0]        # offset -20 / angle -20

    # PID controller #1 params
    # m.kp1 = [0.0, 0.0, 0.9, 0.9, 0.9, 0.0]                # gen/curved surfaces
    m.kp1 = [0.0, 0.0, 0.9, 0.9, 0.0, 0.0]                  # irregular objects / flat surface
    # m.ki1 = [0.0, 0.0, 0.1, 0.1, 0.1, 0.0]                # general/curved surfaces
    m.ki1 = [0.0, 0.0, 0.1, 0.1, 0.0, 0.0]                  # irregular objects / flat surface
    m.ei_min1 = [-5.0, -5.0, -5.0, -25.0, -25.0, -25.0]     # integral error clipping
    m.ei_max1 = [5.0, 5.0, 5.0, 25.0, 25.0, 25.0]           # integral error clipping
    m.alpha1 = 0.5                                          # differential error filtering coeff
    m.ref_sensor_pose = [0.0, 0.0, 3.0, 0.0, 1.0, 0.0]      # flat surface ref pose
    # m.ref_sensor_pose = [0.0, 0.0, 3.0, 0.0, 3.0, 0.0]    # convex surface ref pose
    # m.ref_sensor_pose = [0.0, 0.0, 3.0, 0.0, 0.0, 0.0]    # concave surface ref pose

    # PID controller #2 params
    m.kp2 = 0.2             # proportional gain
    m.kd2 = 0.5             # differential gain
    m.ep_min2 = -25.0       # proportional error clipping
    m.ep_max2 = 25.0        # proportional error clipping
    m.alpha2 = 0.5          # differential error filtering coeff
    m.ref_theta = 0.0       # target bearing (in servo frame)

    # Controller safety limits (wrt sensor frame; mm/deg)
    m.sensor_align_max_abs = [5.0, 15.0, 10.0, 15.0, 5.0, 5.0]

    # Target approach zone radius (mm)
    m.rho_min = 60.0

    # Helper function/closure for pre-processing sensor image
    def preprocess(image):
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        image = image[m.crop_y:m.crop_y+m.crop_h, m.crop_x:m.crop_x+m.crop_w]
        image = cv2.medianBlur(image, m.median_blur_kernel_size)
        image = cv2.adaptiveThreshold(image.astype('uint8'), 255, \
                                      cv2.ADAPTIVE_THRESH_GAUSSIAN_C, \
                                      cv2.THRESH_BINARY, \
                                      m.adapt_thresh_block_size, m.adapt_thresh_offset)
        image = cv2.resize(image, (m.resize_w, m.resize_h), interpolation=cv2.INTER_AREA)
        return np.expand_dims(image, axis=-1)

    # Helper function/closure for computing SE(3) error
    def se3_error(y, r):
        y_inv = transform_euler([0, 0, 0, 0, 0, 0], y, m.robot_axes)
        err = inv_transform_euler(r, y_inv, m.robot_axes)
        return err

    # Create missing dirs
    if not os.path.exists(m.expt_dir):
        os.makedirs(m.expt_dir)
    if not os.path.exists(m.sensor_image_dir):
        os.makedirs(m.sensor_image_dir)
    if not os.path.exists(m.camera_image_dir):
        os.makedirs(m.camera_image_dir)
    if not os.path.exists(m.figure_dir):
        os.makedirs(m.figure_dir)

    # Load CNN from file
    model = models.load_model(m.model_dir/"best_model.h5")
    model.summary()

    # Load extrinsic camera params from file and convert to 4x4 homogeneous matrices
    ext = Namespace()
    ext.load(m.extrinsics_dir/"extrinsics.pkl")
    m.ext_rvec = ext.rvec
    m.ext_tvec = ext.tvec
    m.ext_rmat, _ = cv2.Rodrigues(np.array(m.ext_rvec, dtype=np.float64))
    m.t_cam_base = np.hstack((m.ext_rmat, np.array(m.ext_tvec, dtype=np.float64).reshape((-1, 1))))
    m.t_cam_base = np.vstack((m.t_cam_base, np.array((0.0, 0.0, 0.0, 1.0)).reshape(1, -1)))
    m.t_base_cam = np.linalg.pinv(m.t_cam_base)

    # Initialise camera, ArUco detector and tracker
    camera = RSCamera(color_size=m.color_size, color_fps=m.color_fps,
                      depth_size=m.depth_size, depth_fps=m.depth_fps)
    detector = ArUcoDetector(camera, marker_length=m.marker_length, dict_id=m.dict_id)
    tracker = ArUcoTracker(detector, track_attempts=m.track_attempts)

    # Capture intrinsic camera params
    m.color_cam_matrix = camera.color_cam_matrix
    m.color_dist_coeffs = camera.color_dist_coeffs

    # Initialise robot
    robot = AsyncRobot(SyncRobot(RTDEController(ip=m.robot_ip)))

    # Initialise both PID controllers
    pid1 = PIDController(kp=m.kp1, ki=m.ki1, ei_min=m.ei_min1, ei_max=m.ei_max1,
                         alpha=m.alpha1, error=se3_error)
    pid2 = PIDController(kp=m.kp2, kd=m.kd2, ep_min=m.ep_min2, ep_max=m.ep_max2,
                         alpha=m.alpha2)

    # Initialise TacTip sensor
    sensor = AsyncProcessor(CameraStreamProcessorMT(
        camera=CvVideoCamera(source=m.sensor_source, exposure=m.sensor_exposure),
        display=CvVideoDisplay(name='sensor'),
        writer=CvImageOutputFileSeq()))

    # Initialise camera display (for displaying tracked objects/markers)
    camera_display = CvVideoDisplay(name="camera")
    camera_display.open()

    # Compute pixel position of target in camera image
    m.base_target_pose = inv_transform_euler(m.work_target_pose, m.work_frame, m.robot_axes)
    base_target_position_h = np.vstack((m.base_target_pose[:3].reshape((-1, 1)), (1,)))
    camera_target_position_h = np.dot(m.t_cam_base, base_target_position_h).squeeze()
    m.target = camera.project_point_to_pixel(camera_target_position_h[:3])

    ### RUN EXPERIMENT ...

    try:
        # Skip first few sensor frames to give auto-exposure time to adjust
        sensor.process(num_frames=3)

        # Set robot configuration
        robot.axes = m.robot_axes
        robot.tcp = m.robot_tcp

        # Move to home position (specified wrt base frame)
        print("Moving to home position ...")
        robot.linear_speed = 50
        robot.coord_frame = m.base_frame
        robot.move_linear(m.base_home_pose)

        # Move to start position (specified wrt work frame)
        print("Moving to start position ...")
        robot.coord_frame = m.work_frame
        robot.move_linear(m.work_start_pose)

        input("Position object and press RETURN to continue: ")
        print("Starting push sequence ...")

        # Initialise tracking data
        m.sensor_servo, m.servo_target, m.p_y, m.p_z, m.theta, m.rho, m.sensor_align, \
            m.work_align, m.corners, m.ids, m.cam_poses, m.base_poses, m.centroids, \
            m.cam_centroids, m.base_centroids, \
            = [], [], [], [], [], [], [], [], [], [], [], [], [], [], []

        # Move a bit faster during push sequence
        robot.linear_speed = 100

        # Do push sequence ...
        for i in range(m.max_push_steps):
            # Move pusher/sensor forward along its central axis
            robot.coord_frame = m.base_frame
            robot.coord_frame = robot.pose
            robot.move_linear(m.sensor_tap_move[0])

            # Capture and save sensor image (use second frame to avoid camera double-buffering problem)
            sensor_image = sensor.process(num_frames=1, start_frame=1,
                            outfile=m.sensor_image_dir/("image_" + str(i+1) + ".png")).squeeze()
            # cv2.imwrite(m.sensor_image_dir/("image_" + str(i+1) + ".png"), sensor_image)

            # Track ArUco marker
            try:
                tracker.track()
            except (ColorFrameError, DepthFrameError, DistanceError, \
                    NoMarkersDetected, MultipleMarkersDetected) as e:
                print(f"Tracking exception occurred ({type(e)}) - Aborting push sequence")
                break

            # Move pusher/sensor back along central axis of sensor
            robot.move_linear(m.sensor_tap_move[1])

            # Compute marker centroid position and pose in base frame
            cam_centroid = tracker.centroid_position
            base_centroid = None
            if cam_centroid is not None:
                camera_point_h = np.vstack((np.array(cam_centroid).reshape((-1, 1)), (1,)))
                base_centroid = np.dot(m.t_base_cam, camera_point_h).squeeze()[:3]
            cam_pose = tracker.pose
            base_pose = None
            if cam_pose is not None:
                base_pose = np.dot(m.t_base_cam, cam_pose)

            # Capture ArUco tracking data
            m.corners.append(tracker.corners)
            m.ids.append(tracker.ids)
            m.centroids.append(tracker.centroid)
            m.cam_centroids.append(cam_centroid)
            m.base_centroids.append(base_centroid)
            m.cam_poses.append(cam_pose)
            m.base_poses.append(base_pose)

            # Display and save RealSense camera image showing ArUco marker and target
            display_image = tracker.draw_markers(camera.color_image)
            cv2.circle(display_image, center=(int(m.target[0]), int(m.target[1])), radius=20,
                       color=(0, 0, 255), thickness=4, lineType=8, shift=0)
            camera_display.write(display_image)
            cv2.imwrite(str(m.camera_image_dir/("image_" + str(i+1) + ".png")), display_image)

            # Use CNN to predict z, alpha, and beta from sensor image
            pp_sensor_image = preprocess(sensor_image)
            pp_sensor_image = np.expand_dims(pp_sensor_image, axis=0)
            pp_sensor_image = pp_sensor_image.astype(np.float) * m.input_rescale
            obs_sensor_pose = np.zeros(6)
            obs_sensor_pose[2:5] = np.squeeze(model.predict(pp_sensor_image))

            # Compute servo control in sensor frame
            sensor_servo = pid1.update(y=obs_sensor_pose, r=m.ref_sensor_pose)
            m.sensor_servo.append(sensor_servo)

            # Get sensor frame wrt work frame
            robot.coord_frame = m.work_frame
            work_sensor_frame = robot.pose

            # Compute servo control frame wrt work frame
            work_servo_frame = inv_transform_euler(sensor_servo, work_sensor_frame, m.robot_axes)

            # Compute target pose in servo control frame
            servo_target = transform_euler(m.work_target_pose, work_servo_frame, m.robot_axes)
            m.servo_target.append(servo_target)

            # Compute target bearing (theta) and distance (rho) in servo control frame
            p_y, p_z = servo_target[1:3]
            theta = np.degrees(np.arctan2(p_y, p_z))
            rho = np.sqrt(p_y ** 2 + p_z ** 2)

            print((f"Step: {i}, p_y: {float(p_y):.2f}, p_z: {float(p_z):.2f}, "
                   f"theta: {float(theta):.2f}, rho: {float(rho):.2f}"))

            # Capture control data
            m.p_y.append(p_y)
            m.p_z.append(p_z)
            m.theta.append(theta)
            m.rho.append(rho)

            # Disengage target tracking (PID controller 2) inside target approach zone
            if rho < m.rho_min:
                pid2.kp = 0.0
                pid2.ki = 0.0
                pid2.kd = 0.0

            # Compute target alignment control wrt servo control frame
            servo_align = np.zeros(6)
            servo_align[1] = pid2.update(y=theta, r=m.ref_theta)  # correction to align sensor with target

            # Compute target alignment control wrt sensor frame
            sensor_align = inv_transform_euler(servo_align, sensor_servo, m.robot_axes)
            m.sensor_align.append(sensor_align)

            # Compute target alignment control wrt work frame
            work_align = inv_transform_euler(sensor_align, work_sensor_frame, m.robot_axes)
            m.work_align.append(work_align)

            # Terminate push sequence when sensor tip is approximately on target
            if rho <= m.sensor_tip_radius:
                break

            # Pause if any component of the target alignment control exceeds pre-defined limit
            if np.any(np.abs(sensor_align) > m.sensor_align_max_abs):
                with np.printoptions(precision=2, suppress=True):
                    print(f"Alignment control has exceeded max abs value: {sensor_align}")
                response = input("Enter 'q' to quit or RETURN to continue: ")
                if response == 'q':
                    break

            # Apply target alignment control in work frame
            robot.coord_frame = m.work_frame
            robot.move_linear(work_align)

        # Convert tracking data to numpy arrays
        m.sensor_servo = np.array(m.sensor_servo)
        m.servo_target = np.array(m.servo_target)
        m.p_y = np.array(m.p_y)
        m.p_z = np.array(m.p_z)
        m.theta = np.array(m.theta)
        m.rho = np.array(m.rho)
        m.sensor_align = np.array(m.sensor_align)
        m.work_align = np.array(m.work_align)
        m.corners = np.array(m.corners)
        m.ids = np.array(m.ids)
        m.centroids = np.array(m.centroids)
        m.cam_centroids = np.array(m.cam_centroids)
        m.base_centroids = np.array(m.base_centroids)
        m.cam_poses = np.array(m.cam_poses)
        m.base_poses = np.array(m.base_poses)

        input("Remove object and press RETURN to continue: ")

        # Move to home position (specified wrt base frame)
        print("Moving to home position ...")
        robot.linear_speed = 50
        robot.coord_frame = m.base_frame
        robot.move_linear(m.base_home_pose)

        print("Finished!")

    finally:
        # Clean up
        robot.close()
        sensor.close()
        camera.close()
        camera_display.close()

        # Save experiment meta data to file
        m.save(m.expt_dir/"meta.pkl")

    ### POST-EXPERIMENT DATA ANALYSIS

    # Plot and save ArUco marker centroid trajectory
    plt.figure()
    plt.scatter(m.base_centroids[:, 0], m.base_centroids[:, 1], marker='.')
    plt.scatter(m.base_target_pose[0], m.base_target_pose[1], s=80, marker='x', color='r')
    plt.title("ArUco marker centroid trajectory")
    plt.xlabel("x")
    plt.ylabel("y")
    plt.gca().set_aspect('equal', adjustable='box')
    plt.savefig(m.figure_dir/"centroid_trajectory.png")

    # Plot and save ArUco marker pose trajectory
    plt.figure()
    plt.scatter(m.base_poses[:, 0, 3], m.base_poses[:, 1, 3], marker='.')
    plt.scatter(m.base_target_pose[0], m.base_target_pose[1], s=80, marker='x', color='r')
    plt.title("ArUco marker pose trajectory")
    plt.xlabel("x")
    plt.ylabel("y")
    plt.gca().set_aspect('equal', adjustable='box')
    plt.savefig(m.figure_dir/"pose_trajectory.png")

    # Plot and save PID controller 1 history
    m.history1 = pid1.history()
    for i in range(6):
        plt.figure()
        plt.subplot(5, 1, 1)
        fig_title = "Controller 1,  Output " + str(i+1)
        plt.title(fig_title)
        plt.plot(m.history1['t'], m.history1['y'][:,i])
        plt.ylabel("y")
        plt.subplot(5, 1, 2)
        plt.plot(m.history1['t'], m.history1['e'][:,i])
        plt.ylabel("e")
        plt.subplot(5, 1, 3)
        plt.plot(m.history1['t'], m.history1['ei'][:,i])
        plt.ylabel("ei")
        plt.subplot(5, 1, 4)
        plt.plot(m.history1['t'], m.history1['ed'][:,i])
        plt.ylabel("ed")
        plt.subplot(5, 1, 5)
        plt.plot(m.history1['t'], m.history1['u'][:,i])
        plt.ylabel("u")
        plt.tight_layout()
        plt.savefig(os.path.join(m.figure_dir, "controller_1 output_" + str(i+1) + ".png"))

    # Plot and save PID controller 2 history
    m.history2 = pid2.history()
    plt.figure()
    plt.subplot(6, 1, 1)
    fig_title = "Controller 2,  Output 1"
    plt.title(fig_title)
    plt.plot(m.history2['t'], m.history2['y'])
    plt.ylabel("y")
    plt.subplot(6, 1, 2)
    plt.plot(m.history2['t'], m.history2['e'])
    plt.ylabel("e")
    plt.subplot(6, 1, 3)
    plt.plot(m.history2['t'], m.history2['ep'])
    plt.ylabel("ep")
    plt.subplot(6, 1, 4)
    plt.plot(m.history2['t'], m.history2['ei'])
    plt.ylabel("ei")
    plt.subplot(6, 1, 5)
    plt.plot(m.history2['t'], m.history2['ed'])
    plt.ylabel("ed")
    plt.subplot(6, 1, 6)
    plt.plot(m.history2['t'], m.history2['u'])
    plt.ylabel("u")
    plt.tight_layout()
    plt.savefig(m.figure_dir/"controller_2 output_1.png")

    plt.show()


if __name__ == '__main__':
    main()
